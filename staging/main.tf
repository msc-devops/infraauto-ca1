####################################################
# invoking ./modules *.tf files #
####################################################

# because default vars are already defined in ./modules/vars.tf, only the ones that need to be overwritten have
# to be informe here. In this case, staging will use all default values and won't overwrite anything.

module "three_tiered_infrastructure" {
  source = "../modules/"

}