output "alb_end_point" {
  value = module.three_tiered_infrastructure.alb_end_point
}

output "front_end_url"{
  value = module.three_tiered_infrastructure.front_end_url
}