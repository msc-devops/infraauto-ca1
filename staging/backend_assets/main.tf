####################################################
# invoking modules/backend_assets module .tf files #
####################################################

# because default vars are already defined in ./modules/backend_assets/vars.tf, only the ones that need to be
# overwritten have to be informed here. In this case, staging will use all default values and won't overwrite anything.

module "backend_assets" {
  source = "../../modules/backend_assets"

}