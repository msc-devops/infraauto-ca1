####################################################
# invoking modules/backend_assets module .tf files #
####################################################

# because default vars are already defined in ./modules/backend_assets/vars.tf, only the ones that need to be
# overwritten have to be informed here.

module "backend_assets" {
  source = "../../modules/backend_assets"

  S3_BACKEND_BUCKET   =   "backend-bucket-x00180807-prod"
  DYNAMO_BACKEND_NAME =   "terraform-backend-table-prod"

}