####################################################
# invoking ./modules *.tf files #
####################################################

# because default vars are already defined in ./modules/vars.tf, only the ones that need to be overwritten have
# to be informe here.

module "three_tiered_infrastructure" {
  source = "../modules/"

  DEPLOY_ENV                    = "prod"
  # a different stack deployment in the backend would not be a real case scenario.
  # this was done here just to force a differrent look for the default back-end app EBS deploys by default
  EBS_SOLUTION_STACK_NAME       = "64bit Amazon Linux 2 v5.3.0 running Node.js 14"
  MIN_BACKEND_INSTANCES         = "2"
}