# NO VARS HERE, AS TERRAFORM BACKEND DOES NOT ACCEPT VARS.

terraform {
  backend "s3" {
    bucket         = "backend-bucket-x00180807-prod"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "terraform-backend-table-prod"
    encrypt        = true
  }
}