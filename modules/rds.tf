resource "aws_db_subnet_group" "db-subnet-group" {
  name        = "db-subnet-group-${var.DEPLOY_ENV}"
  description = "RDS subnet group"
  subnet_ids  = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
}


resource "aws_db_instance" "db_instance" {
  allocated_storage         = var.DB_ALLOCATED_STORAGE
  max_allocated_storage     = var.DB_MAX_ALLOCATED_STORAGE
  engine                    = var.DB_ENGINE
  engine_version            = var.DB_VERSION
  instance_class            = var.DB_INSTANCE_CLASS
  identifier                = "${var.DEPLOY_ENV}db"
  name                      = "${var.DEPLOY_ENV}db"
  username                  = var.DB_USERNAME
  password                  = var.DB_PASSWORD
  port                      = var.DB_PORT
  db_subnet_group_name      = aws_db_subnet_group.db-subnet-group.name
  multi_az                  = var.DB_MULTI_AZ # Creates a satandby synchronized replica that takes over in case the main instance dies. Set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids    = [aws_security_group.sg-db.id]
  storage_type              = var.DB_STORAGE_TYPE
  backup_retention_period   = var.DB_BACKUP_RETENTION_PERIOD
  backup_window             = var.DB_BACKUP_WINDOW #The daily time range (in UTC) during which automated backups are created if they are enabled
  final_snapshot_identifier = "${var.DEPLOY_ENV}-db-final-snapshot" # final snapshot when executing terraform destroy
  auto_minor_version_upgrade = "true"
  maintenance_window        =  var.MAINTENANCE_WINDOW
  tags = {
    Name = "db-instance"
    Environment = var.DEPLOY_ENV
  }
  # DB instance is 'publicly accessible', however access is controlled by Security Groups
  publicly_accessible = "false"
}
