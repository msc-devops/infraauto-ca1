#########################################
####### ENVIRONMENT DEFINITION ##########
#########################################
variable "DEPLOY_ENV" {
  default = "staging"
}


#########################################
########### PROVIDER -MAIN ##############
#########################################
variable "AWS_PROFILE" {
  default = "tud"
}

variable "REGION" {
  default = "eu-west-1"
}

#########################################
############ ROUTE53 - DNS ##############
#########################################
variable "DOMAIN_NAME" {
  default = "infraautomated.tk"
}

#########################################
########## VPC (NETWORKING) #############
#########################################
variable "MAIN_VPC_CIDR_BLOCK" {
  default = "10.0.0.0/16"
}

variable "SUBNET_PUBLIC_1" {
  default = "10.0.1.0/24"
}

variable "SUBNET_PUBLIC_2" {
  default = "10.0.2.0/24"
}

variable "SUBNET_PUBLIC_3" {
  default = "10.0.3.0/24"
}

variable "SUBNET_PRIVATE_1" {
  default = "10.0.4.0/24"
}

variable "SUBNET_PRIVATE_2" {
  default = "10.0.5.0/24"
}

variable "SUBNET_PRIVATE_3" {
  default = "10.0.6.0/24"
}

 variable "SUBNET_PRIVATE_4" {
  default = "10.0.7.0/24"
}

variable "SUBNET_PRIVATE_5" {
  default = "10.0.8.0/24"
}

variable "SUBNET_PRIVATE_6" {
  default = "10.0.9.0/24"
}



#########################################
########### DATABASE (RDS) ##############
#########################################

variable "DB_ALLOCATED_STORAGE" {
  default = 20
}

variable "DB_MAX_ALLOCATED_STORAGE" {
  default = 40
}

variable "DB_ENGINE" {
  default = "postgres"
}

variable "DB_VERSION" {
  default = "11.8"
}

variable "DB_INSTANCE_CLASS" {
  default = "db.t2.micro"
}

variable "DB_USERNAME" {
  default = "root"
}

variable "DB_PASSWORD" {
  default = "poasdASD98aprytFAKE"
}

variable "DB_PORT" {
  default = "3873"
}

variable "DB_MULTI_AZ" {
  default = "true"
}

variable "DB_STORAGE_TYPE" {
  default = "gp2"
}

variable "DB_BACKUP_RETENTION_PERIOD" {
  default = 30
}

variable "DB_BACKUP_WINDOW" {
  default = "00:30-02:59"
}

variable "MAINTENANCE_WINDOW" {
  default = "sun:03:01-sun:05:00"
}



#########################################
########### SECURITY GROUPS #############
#########################################
variable "BACKEND_INSTANCES_ALLOWED_SSH_SOURCES" {
  default = "0.0.0.0/0"
}

variable "APP_LOAD_BALANCER_ALLOWED_SOURCES" {
  default = "0.0.0.0/0"
}



#########################################
########## ELASTIC BEANSTALK ############
#########################################
variable "EBS_SOLUTION_STACK_NAME" {
  default = "64bit Amazon Linux 2 v3.1.5 running Python 3.7"
}

variable "ENABLE_SPOT_INSTANCES" {
  default = "true"
}

variable "INSTANCE_TYPES_LIST" {
  default = "t2.micro,t3.micro"
}

variable "MIN_ONDEMAND_INSTANCES" {
  default = "1"
}

variable "SPOT_MAX_PRICE" {
  default = "0.039"
}

variable "HEALTH_CHECK_PATH" {
  default = "/health"
}

variable "DEPLOYMENT_BATCH_SIZE" {
  default = "30"
}

variable "DEPLOYMENT_BATCH_SIZE_TYPE" {
  default = "Percentage"
}

variable "ASG_COOLDOWN_TIME" {
  default = "60"
}

variable "MIN_BACKEND_INSTANCES" {
  default = "1"
}

variable "MAX_BACKEND_INSTANCES" {
  default = "5"
}

variable "ASG_METRIC" {
  default = "CPUUtilization"
}

variable "ASG_METRIC_UNIT" {
  default = "Percent"
}

variable "ASG_METRIC_BREACH_DURATION" {
  default = "3"
}

variable "ASG_METRIC_PERIOD" {
  default = "3"
}

variable "ASG_METRIC_LOWER_THRESHOLD" {
  default = "60"
}

variable "ASG_METRIC_UPPER_THRESHOLD" {
  default = "80"
}
