# static hosting
resource "aws_s3_bucket" "static_hosting" {
  bucket = "${var.DEPLOY_ENV}-${var.DOMAIN_NAME}"
  acl = "public-read"

  tags = {
    Name = "Website"
    Environment = var.DEPLOY_ENV
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT","POST"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "PublicReadForGetBucketObjects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.DEPLOY_ENV}-${var.DOMAIN_NAME}/*"
    }
  ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

# www-redirect
resource "aws_s3_bucket" "www-redirect" {
  bucket = "www.${var.DEPLOY_ENV}-${var.DOMAIN_NAME}"
  acl = "public-read"

  website {
    redirect_all_requests_to = "${var.DEPLOY_ENV}-${var.DOMAIN_NAME}"
  }
}