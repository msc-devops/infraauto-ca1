resource "aws_security_group" "sg-backend-fleet" {
  vpc_id      = aws_vpc.main.id
  name        = "ec2 fleet - ${var.DEPLOY_ENV}"
  description = "security group for EC2 instances"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [var.BACKEND_INSTANCES_ALLOWED_SSH_SOURCES]
  }

  ingress{
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.sg-alb.id]
  }

  # hard-coding ALLOW everything in the outbound direction
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-backend-fleet-${var.DEPLOY_ENV}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "sg-alb" {
  vpc_id      = aws_vpc.main.id
  name        = "ALB - ${var.DEPLOY_ENV}"
  description = "security group for ALB"

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks = [var.APP_LOAD_BALANCER_ALLOWED_SOURCES]
  }

  # hard-coding ALLOW everything in the outbound direction
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-alb-${var.DEPLOY_ENV}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "sg-db" {
  vpc_id      = aws_vpc.main.id
  name        = "postgres database - ${var.DEPLOY_ENV}"
  description = "security group for PostgresSQL DB"

  # database tier will accept connections only from ec2 backend instances
  ingress {
    from_port       = var.DB_PORT
    to_port         = var.DB_PORT
    protocol        = "tcp"
    security_groups = [aws_security_group.sg-backend-fleet.id]
  }

  # hard-coding ALLOW everything in the outbound direction
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  tags = {
    Name = "sg-db-${var.DEPLOY_ENV}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

