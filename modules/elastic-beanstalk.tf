resource "aws_elastic_beanstalk_application" "backend-app" {
  name        = "backend-app-${var.DEPLOY_ENV}"
  description = "core backend app"
}

resource "aws_elastic_beanstalk_environment" "beanstalk-core-env" {
  name                = "core-${var.DEPLOY_ENV}"
  application         = aws_elastic_beanstalk_application.backend-app.name
  solution_stack_name = var.EBS_SOLUTION_STACK_NAME
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = aws_vpc.main.id
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${aws_subnet.main-private-1.id},${aws_subnet.main-private-2.id},${aws_subnet.main-private-3.id}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "false"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.app-ec2-role.name
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = aws_security_group.sg-backend-fleet.id
  }

  # SPOT instances configs
  setting {
    # Enable Spot Instance requests for your environment. When false, some options in this namespace don't take effect.
    namespace = "aws:ec2:instances"
    name      = "EnableSpot"
    value     = var.ENABLE_SPOT_INSTANCES
  }

  setting {
//   A comma-separated list of instance types you want your environment to use. For example: t2.micro,t3.micro
//   When Spot Instances are disabled (EnableSpot is false), only the first instance type on the list is used.
//   The first instance type on the list in this option is equivalent to the value of the InstanceType option in the aws:autoscaling:launchconfiguration namespace.
    namespace = "aws:ec2:instances"
    name      = "InstanceTypes"
    value     = var.INSTANCE_TYPES_LIST
  }
  setting {
    # The minimum number of On-Demand Instances that your Auto Scaling group provisions before considering Spot Instances as your environment scales up.
    namespace = "aws:ec2:instances"
    name      = "SpotFleetOnDemandBase"
    value     = var.MIN_ONDEMAND_INSTANCES
  }

  setting {
    # The maximum price per unit hour, in US$, that you're willing to pay for a Spot Instance.
    namespace = "aws:ec2:instances"
    name      = "SpotMaxPrice"
    value     = var.SPOT_MAX_PRICE
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = aws_iam_role.elasticbeanstalk-service-role.name
  }

  #Application Load Balancer
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "application"
  }
  # the default healthcheck path
  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "HealthCheckPath"
    value     = var.HEALTH_CHECK_PATH
  }
  # the ALB will be seating in Public VPC.
  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "public"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${aws_subnet.main-public-1.id},${aws_subnet.main-public-2.id},${aws_subnet.main-public-3.id}"
  }
  # disabling port 80 in the Load Balancer
  setting {
    namespace = "aws:elbv2:listener:default"
    name      = "ListenerEnabled"
    value     = "true"
  }

  setting {
    # Percentage or fixed number of Amazon EC2 instances in the Auto Scaling group on which to simultaneously perform deployments. Valid values vary per BatchSizeType setting.
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSize"
    value     = var.DEPLOYMENT_BATCH_SIZE
  }
  setting {
    # The type of number that is specified in BatchSize.
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSizeType"
    value     = var.DEPLOYMENT_BATCH_SIZE_TYPE
  }
  setting {
    # whenever the autoscaling group decides to scale up, it will create a new ec2 instance randomly across all three AZs.
    namespace = "aws:autoscaling:asg"
    name      = "Availability Zones"
    value     = "Any 3"
  }
  setting {
    # Cooldown periods help prevent Amazon EC2 Auto Scaling from initiating additional scaling activities before the effects of previous activities are visible.
    # A cooldown period is the amount of time, in seconds, after a scaling activity completes before another scaling activity can start.
    namespace = "aws:autoscaling:asg"
    name      = "Cooldown"
    value     = var.ASG_COOLDOWN_TIME
  }
  setting {
    # Minimum number of instances you want in your Auto Scaling group.
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = var.MIN_BACKEND_INSTANCES
  }
  setting {
    # Maximum number of instances you want in your Auto Scaling group.
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = var.MAX_BACKEND_INSTANCES
  }
  setting {
    # Time-based rolling updates apply a PauseTime between batches.
    # Health-based rolling updates wait for new instances to pass health checks before moving on to the next batch.
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = "Health"
  }
  setting {
    # Metric used for your Auto Scaling trigger.
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = var.ASG_METRIC
  }
  setting {
    # Unit for the trigger measurement
    namespace = "aws:autoscaling:trigger"
    name      = "Unit"
    value     = var.ASG_METRIC_UNIT
  }
  setting {
    # Amount of time, in minutes, a metric can be beyond its defined limit
    # (as specified in the UpperThreshold and LowerThreshold) before the trigger fires.
    namespace = "aws:autoscaling:trigger"
    name      = "BreachDuration"
    value     = var.ASG_METRIC_BREACH_DURATION
  }
  setting {
    # Specifies how frequently Amazon CloudWatch measures the metrics for your trigger.
    # The value is the number of minutes between two consecutive periods.
    namespace = "aws:autoscaling:trigger"
    name      = "Period"
    value     = var.ASG_METRIC_PERIOD
  }
  setting {
    # If the measurement falls below this number for the breach duration, a trigger is fired.
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = var.ASG_METRIC_LOWER_THRESHOLD
  }
  setting {
    # If the measurement is higher than this number for the breach duration, a trigger is fired.
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = var.ASG_METRIC_UPPER_THRESHOLD
  }


//  setting {
//    namespace = "aws:elbv2:listener:443"
//    name      = "SSLCertificateArns"
//    value     = aws_acm_certificate.https_cert.arn
//  }
//
//  depends_on = [aws_internet_gateway.main-gw]

}

output "alb_end_point" {
  value = aws_elastic_beanstalk_environment.beanstalk-core-env.endpoint_url
}