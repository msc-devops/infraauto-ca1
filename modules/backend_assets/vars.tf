variable "AWS_PROFILE" {
  default = "tud"
}

variable "REGION" {
  default = "eu-west-1"
}

variable "S3_BACKEND_BUCKET" {
  default = "backend-bucket-x00180807-staging"
}

variable "S3_BACKEND_SSE_ALGORITHM" {
  default = "AES256"
}

variable "DYNAMO_BACKEND_NAME" {
  default = "terraform-backend-table-staging"
}