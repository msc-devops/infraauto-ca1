provider "aws" {
  region    = var.REGION
  profile   = var.AWS_PROFILE
}

