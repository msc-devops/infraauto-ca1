# AVOIDING VARS HERE, BECAUSE BACKEND DOES NOT ACCEPT VARS.

resource "aws_s3_bucket" "terraform_state" {
  bucket = var.S3_BACKEND_BUCKET
  # versioning will allow to keep the history of the IaC from terraform's point of view.
  versioning {
    enabled = true
  }
  # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.S3_BACKEND_SSE_ALGORITHM
      }
    }
  }
}

