# iam roles
resource "aws_iam_role" "app-ec2-role" {
  name               = "app-ec2-role-${var.DEPLOY_ENV}"
  assume_role_policy = <<-EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

# service
resource "aws_iam_role" "elasticbeanstalk-service-role" {
  name = "elasticbeanstalk-service-role-${var.DEPLOY_ENV}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}


# Adding policies to app-ec2-role
resource "aws_iam_policy_attachment" "app-attach1" {
  name       = "app-attach1-${var.DEPLOY_ENV}"
  roles      = [aws_iam_role.app-ec2-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_policy_attachment" "app-attach3" {
  name       = "app-attach3-${var.DEPLOY_ENV}"
  roles      = [aws_iam_role.app-ec2-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}

# Adding policies to elasticbeanstalk-service-role

resource "aws_iam_policy_attachment" "app-attach4" {
  name       = "app-attach4-${var.DEPLOY_ENV}"
  roles      = [aws_iam_role.elasticbeanstalk-service-role.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"

  depends_on = [aws_iam_role.elasticbeanstalk-service-role]
}


resource "aws_iam_policy_attachment" "app-attach5" {
  name       = "app-attach5-${var.DEPLOY_ENV}"
  roles      = [aws_iam_role.elasticbeanstalk-service-role.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
  depends_on = [aws_iam_role.elasticbeanstalk-service-role]
}

# instance profile
resource "aws_iam_instance_profile" "app-ec2-role" {
  name = "app-ec2-role-${var.DEPLOY_ENV}"
  role = aws_iam_role.app-ec2-role.name
}
